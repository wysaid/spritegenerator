<h1> SpriteGenerator </h1>

<p><b>A Free Image Merger</b></p>

<pre>There're three projects:
The ".dsw" is for vc6.0 --What's more: if your vc6.0 doesn't work with it, try to upgrade your IDE! It's out of date!
The ".v9.sln" is for vs2008
The ".v11.sln" is for vs2012

If you're using VS2010, you can also use the ".v9.sln" ver.
</pre><br>
<p><pre>The SpriteGenerator provides small image merge with alpha channel.
With SpriteGenerator, you can simply make all your small images in a big one.
It will generate a configuration file, which describes the relative positions of the small images.
It is nice for the texture images of GLSL shaders.
</pre></p>

<h3> ShortCut </h3>
<p><img src="https://raw.github.com/wysaid/SpriteGenerator/master/shortcut1.jpg"></p>
<p><img src="https://raw.github.com/wysaid/SpriteGenerator/master/shortcut2.jpg"></p>